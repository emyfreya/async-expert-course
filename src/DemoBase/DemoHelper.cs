﻿using System.ComponentModel;
using System.Reflection;

namespace DemoBase;

public static class DemoHelper
{
    public static readonly string[] LookupMethodNames = {
            "StartAsync",
            "Start"
        };

    private static readonly string Separator = new('-', 35);

    public static void WriteInformation(string text)
    {
        Write($"Information : {text}", ConsoleColor.Green);
    }

    public static void WriteError(string text)
    {
        Write($"Error : {text}", ConsoleColor.Red);
    }

    private static void Write(string text, ConsoleColor foreground)
    {
        Console.ForegroundColor = foreground;
        Console.WriteLine($"[{DateTime.Now:HH:mm:ss.ffffff}] {text}");
        Console.ResetColor();
    }

    public static async Task LookupAsync(string[] args)
    {
        var types = Assembly.GetExecutingAssembly()
            .GetTypes()
            .Select(e =>
                new
                {
                    Type = e,
                    e.Name,
                    e.GetCustomAttribute<DescriptionAttribute>()?.Description
                })
            .Where(e => e.Name.EndsWith("Demo"))
            .OrderBy(e => e.Name)
            .ToList();

        while (true)
        {
            foreach (var item in types)
            {
                Write($"[{types.IndexOf(item)}] {item.Name}", ConsoleColor.Blue);
                Write($"\t{item.Description}", ConsoleColor.DarkGray);
            }

            WriteInformation($"Enter a number between 0 and {types.Count - 1}");
            string? input = Console.ReadLine();
            Console.WriteLine();

            if (!int.TryParse(input, out int choiceNumber) || (choiceNumber > types.Count - 1 || choiceNumber < 0))
            {
                WriteError("Invalid input");
                Console.WriteLine();

                continue;
            }

            Console.Clear();

            MethodInfo? method = null;
            var choice = types[choiceNumber];

            foreach (string lookupMethodName in LookupMethodNames)
            {
                method = choice.Type.GetMethod(lookupMethodName);

                if (method != null)
                {
                    break;
                }
            }

            if (method == null)
            {
                continue;
            }

            if (!method.IsStatic)
            {
                throw new InvalidOperationException("Method must be static.");
            }

            WriteInformation($"Starting demo {choice.Name}");

            object? methodValue = method.Invoke(null, null);

            if (methodValue is Task task)
            {
                await task;
            }

            WriteInformation($"End demo {choice.Name}");
            Console.ReadKey();
        }
    }
}
