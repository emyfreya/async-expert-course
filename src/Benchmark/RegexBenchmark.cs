﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using System.Text.RegularExpressions;

namespace AsyncExpert.Benchmark
{
    [SimpleJob(RuntimeMoniker.NetCoreApp31)]
    [SimpleJob(RuntimeMoniker.NetCoreApp50)]
    [MarkdownExporter]
    [CsvMeasurementsExporter]   // Generates a graphic with plots.
    [RPlotExporter]             // 
    public class RegexBenchmark
    {
        private readonly Regex _regex = new Regex("[a-zA-Z0-9]*", RegexOptions.Compiled);

        [Benchmark]
        public bool IsMatch()
        {
            return _regex.IsMatch("abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        }
    }
}