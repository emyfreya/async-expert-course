﻿using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Loggers;
using BenchmarkDotNet.Reports;
using BenchmarkDotNet.Running;
using System.Collections.Generic;
using System.Reflection;

namespace AsyncExpert.Benchmark
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Summary r1 = BenchmarkRunner.Run<FakeServiceBenchmark>();
            // Summary r2 = BenchmarkRunner.RunUrl("https://gist.github.com/any/online/available/benchmark");

            ManualConfig config = ManualConfig.CreateEmpty()
                .AddLogger(new ConsoleLogger())
                .AddExporter(MarkdownExporter.Default);

            IEnumerable<Summary> r3 = BenchmarkSwitcher.FromAssembly(Assembly.GetExecutingAssembly()).Run(args, config);
        }
    }
}