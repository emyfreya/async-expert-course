﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsyncExpert.Benchmark.Services
{
    public class FakeService
    {
        private List<string> _data = new List<string>();

        public Task InstantCallAsync() => Task.CompletedTask;

        public async Task CallDarthVaderAsync()
        {
            await Task.Run(() =>
            {
                for (int i = 0; i < 20000; i++)
                {
                    _data.Add($"Data #{i}");
                }
            });
        }

        public async Task CallAnakinAsync()
        {
            await Task.Delay(1);
        }
    }
}
