﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace DemoApp
{
    public class SyncService : IAsyncInterface
    {
        /// <inheritdoc />
        public Task<int> GetAsync(CancellationToken cancellationToken)
        {
            try
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    return Task.FromCanceled<int>(cancellationToken);
                }

                return Task.FromResult(ThirdPartyApi.Get());
            }
            catch (Exception e)
            {
                return Task.FromException<int>(e);
            }
        }
    }
}
