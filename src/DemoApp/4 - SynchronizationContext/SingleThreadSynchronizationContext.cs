﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace DemoApp
{
    public class SingleThreadSynchronizationContext : System.Threading.SynchronizationContext
    {
        private readonly BlockingCollection<(SendOrPostCallback, object?)> _queue = new BlockingCollection<(SendOrPostCallback, object?)>();

        /// <inheritdoc />
        public override void Post(SendOrPostCallback d, object? state)
        {
            _queue.Add((d, state));
        }

        public void RunOnCurrentThread()
        {
            var thread = Thread.CurrentThread;
            string threadType = thread.IsThreadPoolThread ? "ThreadPool" : "Not ThreadPool";
            Console.WriteLine($"RunOnCurrentThread {thread.ManagedThreadId} : {threadType}");

            SetSynchronizationContext(this);

            foreach ((SendOrPostCallback callback, object? state) in _queue.GetConsumingEnumerable())
            {
                callback(state);
            }
        }
    }
}
