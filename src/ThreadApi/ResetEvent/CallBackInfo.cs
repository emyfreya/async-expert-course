﻿namespace ThreadApi.ResetEvent;

public class CallBackInfo
{
    public RegisteredWaitHandle? Handle;
}