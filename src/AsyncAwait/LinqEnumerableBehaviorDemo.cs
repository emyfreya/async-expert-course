﻿using System.ComponentModel;

namespace AsyncAwait;

[Description("Linq Task Enumerable Behavior")]
public class LinqEnumerableBehaviorDemo
{
    public static async Task StartAsync()
    {
        Console.WriteLine("Without explicit array building");

        var tasks = Enumerable.Range(0, 3)
            .Select(i =>
            {
                Console.WriteLine(i);

                return Task.Delay(1000);
            });

        foreach (Task task in tasks)
        {
            await task;
        }

        Console.WriteLine("With explicit array building");

        Task[] tasksArray = Enumerable.Range(0, 3)
            .Select(i =>
            {
                Console.WriteLine(i);

                return Task.Delay(1000);
            })
            .ToArray();

        foreach (Task task in tasksArray)
        {
            await task;
        }
    }
}